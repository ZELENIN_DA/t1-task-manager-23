package ru.t1.dzelenin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(String userId, String name, String description);

    @NotNull
    Project create(String userId, String name);

}


