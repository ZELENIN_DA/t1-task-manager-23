package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.repository.ICommandRepository;
import ru.t1.dzelenin.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService extends ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getCommands();

    void add(@NotNull AbstractCommand command);

    @NotNull
    AbstractCommand getCommandByName(String name);

    @NotNull
    AbstractCommand getCommandByArgument(String argument);

}
