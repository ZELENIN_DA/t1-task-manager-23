package ru.t1.dzelenin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    Task create(String userId, String name, String description);

    @NotNull
    Task create(String userId, String name);

    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}



