package ru.t1.dzelenin.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import lombok.Getter;

@Getter
public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    private String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}

