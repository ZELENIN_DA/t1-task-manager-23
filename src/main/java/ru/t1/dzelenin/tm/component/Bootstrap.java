package ru.t1.dzelenin.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import ru.t1.dzelenin.tm.api.repository.ICommandRepository;
import ru.t1.dzelenin.tm.api.repository.IProjectRepository;
import ru.t1.dzelenin.tm.api.repository.ITaskRepository;
import ru.t1.dzelenin.tm.api.repository.IUserRepository;
import ru.t1.dzelenin.tm.api.service.*;
import ru.t1.dzelenin.tm.command.AbstractCommand;
import ru.t1.dzelenin.tm.command.project.*;
import ru.t1.dzelenin.tm.command.system.*;
import ru.t1.dzelenin.tm.command.task.*;
import ru.t1.dzelenin.tm.command.user.*;
import ru.t1.dzelenin.tm.enumerated.Role;
import ru.t1.dzelenin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.dzelenin.tm.exception.system.CommandNotSupportedException;
import ru.t1.dzelenin.tm.model.User;
import ru.t1.dzelenin.tm.repository.CommandRepository;
import ru.t1.dzelenin.tm.repository.ProjectRepository;
import ru.t1.dzelenin.tm.repository.TaskRepository;
import ru.t1.dzelenin.tm.repository.UserRepository;
import ru.t1.dzelenin.tm.service.*;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    @Getter
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();


    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationListCommand());
        registry(new ApplicationVersionCommand());
        registry(new ApplicationHelpCommand());
        registry(new ArgumentListConnamd());
        registry(new SystemInfoCommand());
        registry(new ApplicationExitCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserRemoveCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());

        registry(new ProjectChangeStatusIdCommand());
        registry(new ProjectChangeStatusIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusIdCommand());
        registry(new TaskChangeStatusIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
    }


    private void initLogger() {
        loggerService.info("** WELCOME TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });

    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }


    public void start(@NotNull final String[] args) {
        initLogger();
        initDemoData();
        if (args.length == 0 || args == null)
            runByCommand();
        else
            runByArgument(args);
    }

    private void runByArgument(String[] args) {
        try {
            processArguments(args);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("[FAIL]");
        } finally {
            exit();
        }
    }

    private void runByCommand() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    private void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }


    private void exit() {
        System.exit(0);
    }

    private void initDemoData() {
        User user1 = userService.create("1", "1", "user01@address.ru");
        User user2 = userService.create("2", "2", Role.ADMIN);
        User user3 = userService.create("3", "3", "user03@address.ru");

        projectService.create("PROJECT_01", "Test project 1");
        projectService.create("PROJECT_02", "Test project 2");
        projectService.create("PROJECT_03", "Test project 4");
        projectService.create("PROJECT_04", "Test project 5");

        taskService.create("TASK_01", "Test task 1");
        taskService.create("TASK_02", "Test task 2");
        taskService.create("TASK_03", "Test task 3");
        taskService.create("TASK_04", "Test task 4");
    }

}
