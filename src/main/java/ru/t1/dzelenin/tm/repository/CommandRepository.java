package ru.t1.dzelenin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.repository.ICommandRepository;
import ru.t1.dzelenin.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return mapByName.values();
    }

    @NotNull
    @Override
    public void add(@NotNull final AbstractCommand command) {
        if (command == null) return;
        final String name = command.getName();
        if (name != null && !name.isEmpty()) mapByName.put(name, command);
        final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, command);
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        if (name == null || name.isEmpty()) return null;
        return mapByName.get(name);
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByArgument(@NotNull final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return mapByArgument.get(argument);
    }

}
