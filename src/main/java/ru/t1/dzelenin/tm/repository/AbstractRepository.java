package ru.t1.dzelenin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.IRepository;
import ru.t1.dzelenin.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;


public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @NotNull
    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @NotNull
    @Override
    public M removeOne(@NotNull final M model) {
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @NotNull
    @Override
    public void removeAll(@NotNull Collection<M> collection) {
        models.removeAll(collection);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @NotNull
    @Override
    public void removeAll() {
        models.clear();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return models
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        return models.get(index);
    }

    @NotNull
    @Override
    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @NotNull
    @Override
    public Integer getSize() {
        return models.size();
    }

}


