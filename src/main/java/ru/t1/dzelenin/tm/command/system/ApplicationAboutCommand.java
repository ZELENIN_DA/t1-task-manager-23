package ru.t1.dzelenin.tm.command.system;

import org.jetbrains.annotations.NotNull;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Daniil Zelenin");
        System.out.println("email: dzelenin@t1-consulting.ru");
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show developer info.";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }
}
