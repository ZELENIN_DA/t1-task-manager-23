package ru.t1.dzelenin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.model.Project;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectShowCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Project project = getProjectService().findOneById(getUserId(), id);
        showProject(project);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display project by id.";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
