package ru.t1.dzelenin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.enumerated.Role;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-remove";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getDescription() {
        return "user remove";
    }

}
