package ru.t1.dzelenin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.service.IAuthService;
import ru.t1.dzelenin.tm.api.service.IUserService;
import ru.t1.dzelenin.tm.enumerated.Role;
import ru.t1.dzelenin.tm.exception.field.LoginEmptyException;
import ru.t1.dzelenin.tm.exception.field.PasswordEmptyException;
import ru.t1.dzelenin.tm.exception.user.AccessDeniedException;
import ru.t1.dzelenin.tm.exception.user.LockedUserException;
import ru.t1.dzelenin.tm.exception.user.PermissionException;
import ru.t1.dzelenin.tm.model.User;
import ru.t1.dzelenin.tm.util.HashUtil;

import java.util.Arrays;


public class AuthService implements IAuthService {

    private final IUserService userService;

    @NotNull
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @NotNull
    @Override
    public User registry(@NotNull final String login, @NotNull final String password, final String email) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public void login(@NotNull final String login, @NotNull final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.isLocked()) throw new LockedUserException();
        String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @NotNull
    @Override
    public void logout() {
        userId = null;
    }

    @NotNull
    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @NotNull
    @Override
    public void checkRoles(@NotNull Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        @NotNull final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}

