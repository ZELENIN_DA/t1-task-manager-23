package ru.t1.dzelenin.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class CommandConst {

    @NotNull
    public static final String HELP = "help";

    @NotNull
    public static final String VERSION = "version";

    @NotNull
    public static final String ABOUT = "about";

    @NotNull
    public static final String EXIT = "exit";

    @NotNull
    public static final String INFO = "info";

    @NotNull
    public static final String COMMANDS = "commands";

    @NotNull
    public static final String ARGUMENTS = "arguments";

    @NotNull
    public static final String PROJECT_LIST = "project-list";

    @NotNull
    public static final String PROJECT_CREATE = "project-create";

    @NotNull
    public static final String PROJECT_CLEAR = "project-clear";

    @NotNull
    public static final String PROJECT_SHOW_BY_ID = "project-show-id";

    @NotNull
    public static final String PROJECT_SHOW_BY_INDEX = "project-show-index";

    @NotNull
    public static final String PROJECT_UPDATE_BY_ID = "project-update-id";

    @NotNull
    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-index";

    @NotNull
    public static final String PROJECT_REMOVE_BY_ID = "project-delete-id";

    @NotNull
    public static final String PROJECT_REMOVE_BY_INDEX = "project-delete-index";

    @NotNull
    public static final String PROJECT_START_BY_ID = "project-start-id";

    @NotNull
    public static final String PROJECT_START_BY_INDEX = "project-start-index";

    @NotNull
    public static final String PROJECT_COMPLETE_BY_ID = "project-complete-id";

    @NotNull
    public static final String PROJECT_COMPLETE_BY_INDEX = "project-complete-index";

    @NotNull
    public static final String PROJECT_CHANGE_STATUS_BY_ID = "project-change-status-id";

    @NotNull
    public static final String PROJECT_CHANGE_STATUS_BY_INDEX = "project-change-status-index";

    @NotNull
    public static final String TASK_LIST = "task-list";

    @NotNull
    public static final String TASK_CREATE = "task-create";

    @NotNull
    public static final String TASK_CLEAR = "task-clear";

    @NotNull
    public static final String TASK_SHOW_BY_ID = "task-show-id";

    @NotNull
    public static final String TASK_SHOW_BY_INDEX = "task-show-index";

    @NotNull
    public static final String TASK_UPDATE_BY_ID = "task-update-id";

    @NotNull
    public static final String TASK_UPDATE_BY_INDEX = "task-update-index";

    @NotNull
    public static final String TASK_REMOVE_BY_ID = "task-delete-id";

    @NotNull
    public static final String TASK_REMOVE_BY_INDEX = "task-delete-index";

    @NotNull
    public static final String TASK_START_BY_ID = "task-start-id";

    @NotNull
    public static final String TASK_START_BY_INDEX = "task-start-index";

    @NotNull
    public static final String TASK_COMPLETE_BY_ID = "task-complete-id";

    @NotNull
    public static final String TASK_COMPLETE_BY_INDEX = "task-complete-index";

    @NotNull
    public static final String TASK_CHANGE_STATUS_BY_ID = "task-change-status-id";

    @NotNull
    public static final String TASK_CHANGE_STATUS_BY_INDEX = "task-change-status-index";

    @NotNull
    public static final String TASK_BIND_TO_PROJECT = "task-bind-to-project";

    @NotNull
    public static final String TASK_UNBIND_FROM_PROJECT = "task-unbind-from-project";

    @NotNull
    public static final String TASK_SHOW_BY_PROJECT_ID = "task-show-by-project-id";

}
